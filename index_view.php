<?php
    require 'vendor/autoload.php';
    $validator = new Valitron\Validator($_POST);
    $validator -> rules([
            'requiredWithout' => [
                    ['token', ['email', 'password'], true]
            ],
            'requiredWith' => [
                    ['password', ['email']]
            ],
            'email' => [
                    ['email']
            ],
            'optional' => [
                    ['email']
            ]
    ]);
    if($validator -> validate()){
        echo "That`s OK!";
    } else {
        print_r($validator -> errors());
    }
?>

<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
</head>
<body>

    <style>
        form {
            display: flex;
            align-items: center;
            flex-direction: column;
            max-width: 500px;
            height:100%;
        }
        input {
            width:100%;
            height:35px;
            margin: 15px 0;
            padding: 0 15px;
        }
        input:last-child {
            font: 16px 'Impact';
            background: forestgreen;
            border: none;
        }
    </style>

    <form action="" method="post">
        <input type="text" placeholder="Name" name="name" maxlength="15" required>
        <input type="email" placeholder="E-mail" name="mail" required>
        <input type="tel" placeholder="+373-11-111-111" name="tel" required>
        <input type="number" placeholder="Age" name="age" required>
        <input type="date" placeholder="Birthday" name="birthday" minlength="0" maxlength="100" required>
        <input type="password" placeholder="Password" name="password" maxlength="20" required>
        <input type="submit">
    </form>
</body>
</html>